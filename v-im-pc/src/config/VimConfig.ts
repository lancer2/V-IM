interface VimConfig {
  clientId: string;
  clientSecret: string;
  host: string;
  httProtocol: string;
  wsProtocol: string;
  scope: string;
  httPort: number;
  wsPort: number;
  client: string;
}

const vimConfig: VimConfig = {
  clientId: "app",
  clientSecret: "app",
  host: "101.200.151.183",
  httProtocol: "http",
  wsProtocol: "ws",
  scope: "server",
  httPort: 8080,
  wsPort: 9326,
  client: "pc",
};
export default vimConfig;
